# EftlAngular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.1.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

# Mise en place

## Premiers modules

```
ng g module core
ng g module shared
ng g module ui
ng g module icons
ng g module templates
```

```
ng g module orders --routing
ng g module login --routing
ng g module clients --routing
ng g module PageNotFound --routing
```

```
git commit -m "1-Mise en place des modules"
```

## Création des premières pages

```
ng g component clients/pages/PageListClients
ng g component clients/pages/PageEditClient
ng g component clients/pages/PageAddClient

ng g component login/pages/PageSignIn
ng g component login/pages/PageSignUp
ng g component login/pages/PageResetPassword
ng g component login/pages/PageForgotPassword
```

## Correction exercice
```
ng g component orders/pages/PageListOrders
ng g component orders/pages/PageAddOrder
ng g component orders/pages/PageEditOrder
ng g component page-not-found/pages/PageNotFound
```


```
git commit -m "3-Mise en place des pages"
```

## UI Component

ng g component ui/components/Ui

## Layout

```
ng g component core/components/Nav
ng g component core/components/Footer
ng g component core/components/Header --export
```


# Layout 2

ng g component core/components/Nav2 --export

ng g component core/components/Footer2 --export

ng g component core/components/Header2 --export


ng g component ui/components/Ui2 --export


# Icones
```
ng g component icons/components/IconNav --export

ng g component icons/components/IconEdit --export

ng g component icons/components/IconDelete --export

ng g component icons/components/IconClose --export
```


ng add @fortawesome/angular-fontawesome
