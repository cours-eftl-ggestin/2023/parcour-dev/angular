
# Observable

Changement du type de version, utilisation d'un format sementique

type de la version a utilisation : 
```ts
export interface Version {
    major: number;
    minor: number;
    patch: number;
}
```

1- modification du service pour gere le nouveau type
2- modification du component footer pour afficher la version comme ceci => v1.0.0
3- modification du service pour modifier les 3 valeurs de facon independante
4- ajouter des boutons pour modifier les valeurs de la version dans le app.component.html
