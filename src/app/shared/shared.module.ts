import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TemplatesModule} from '../templates/templates.module';
import {IconsModule} from '../icons/icons.module';
import {UiModule} from '../ui/ui.module';
import {NavButtonComponent} from './components/nav-button/nav-button.component';
import {RouterModule} from "@angular/router";
import {HttpClientModule} from "@angular/common/http";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";


@NgModule({
  declarations: [
    NavButtonComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    NavButtonComponent,
    TemplatesModule,
    IconsModule,
    UiModule,
    HttpClientModule,
    FontAwesomeModule
  ]
})
export class SharedModule {
}
