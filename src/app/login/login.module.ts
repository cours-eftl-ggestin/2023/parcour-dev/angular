import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { PageSignUpComponent } from './pages/page-sign-up/page-sign-up.component';
import { PageResetPasswordComponent } from './pages/page-reset-password/page-reset-password.component';
import { PageForgotPasswordComponent } from './pages/page-forgot-password/page-forgot-password.component';
import { PageSignInComponent } from './pages/page-sign-in/page-sign-in.component';
import { SharedModule } from '../shared/shared.module';
import {CoreModule} from "../core/core.module";
import {FooterComponent} from "../core/components/footer/footer.component";


@NgModule({
  declarations: [
    FooterComponent,
    PageSignUpComponent,
       PageResetPasswordComponent,
       PageForgotPasswordComponent,
       PageSignInComponent
  ],
    imports: [
        CommonModule,
        LoginRoutingModule, // Pour défi : afficher le composant
        SharedModule,
    ],
  exports: [
    PageSignInComponent,
    PageSignUpComponent
  ]
})
export class LoginModule { }
