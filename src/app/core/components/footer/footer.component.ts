import {Component} from '@angular/core';
import {VersionService} from "../../services/version.service";
import {map, Observable} from "rxjs";

@Component({
  selector: 'app-footer',
  template: `Version : {{ version$ | async }}`,
})
export class FooterComponent {

  version$: Observable<string> = this.versionService.version$
    // .pipe(map(value => 'v' + value.major + '.' + value.minor + '.' + value.patch));
    .pipe(map(value => `v${value.major}.${value.minor}.${value.patch}`));

  // Injection du service de version
  constructor(
    private versionService: VersionService,
  ) {
  }
}
