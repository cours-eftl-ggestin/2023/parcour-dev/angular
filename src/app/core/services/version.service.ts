import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {Version} from "../models/version";

@Injectable({
  providedIn: 'root'
})
export class VersionService {

  // Definition d'un behaviorSubject pour stocker la version de l'application
  private version: BehaviorSubject<Version> = new BehaviorSubject<Version>({
   major: 1,
   minor: 0,
   patch: 0
  });

  // Exposition de la version de l'application via un observable
  get version$(): Observable<Version> {
    return this.version.asObservable();
  }

  constructor() { }

  // Incrementation de la version de l'application
  incrementVersion(props: 'major' | 'minor' | 'patch'): void {
    this.version.next({
      ...this.version.getValue(),
      [props]: this.version.getValue()[props] + 1,
    });
  }

  incrementVersionMajor() {
    const oldVersion: Version = this.version.getValue();
    const newVersion: Version = {
      major: oldVersion.major + 1,
      minor: oldVersion.minor,
      patch: oldVersion.patch
    }
    this.version.next(newVersion);
  }

  incrementVersionMinor() {
    const oldVersion: Version = this.version.getValue();
    const newVersion: Version = {
      ...oldVersion,
      minor: oldVersion.minor + 1,
    }
    this.version.next(newVersion);
  }

  incrementVersionPatch() {
    this.version.next({
      ...this.version.getValue(),
      patch: this.version.getValue().patch + 1,
    });
  }
}
