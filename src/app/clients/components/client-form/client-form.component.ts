import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, Validators} from "@angular/forms";
import {Clients} from "../../models/clients";

interface ClientForm {
  name: FormControl<string | null>;
  revenue: FormControl<number | null>;
  status: FormControl<string | null>;
  comments: FormControl<string | null>;
}

@Component({
  selector: 'app-client-form',
  templateUrl: './client-form.component.html',
  styleUrls: ['./client-form.component.scss']
})
export class ClientFormComponent implements OnInit {
  @Input() client!: Clients;

  @Output() cancel: EventEmitter<void> = new EventEmitter<void>();
  @Output() validate: EventEmitter<Clients> = new EventEmitter<Clients>();

  titleButton = 'Ajouter';
  creationOnGoing = false;

  public form = this.fb.group<ClientForm>({
    name: this.fb.control<string | null>(null, [Validators.required]),
    revenue: this.fb.control<number | null>(null, [Validators.required]),
    status: this.fb.control<string>('Actif', [Validators.required]),
    comments: this.fb.control<string | null>(null),
  });

  constructor(
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
    if (this.client) {
      this.titleButton = 'Modifier';
      this.form.patchValue(this.client);
    }
  }

  isNotValid() {
    return this.form.pristine || !this.form.valid;
  }

  isDisabled() {
    return this.isNotValid() && !this.creationOnGoing;
  }

  submit() {
    this.creationOnGoing = true;
    const client: Clients = {...this.form.value} as Clients;
    this.validate.emit(client);
  }

  cancelAction() {
    this.cancel.emit();
  }

}
