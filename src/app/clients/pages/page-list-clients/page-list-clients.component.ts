import {Component} from '@angular/core';
import {Clients} from "../../models/clients";
import {ClientsService} from "../../services/clients.service";
import {Observable} from "rxjs";

@Component({
  selector: 'app-page-list-clients',
  templateUrl: './page-list-clients.component.html',
  styleUrls: ['./page-list-clients.component.scss']
})
export class PageListClientsComponent {
  clients$: Observable<Clients[]> = this.clientsService.getClients();

  constructor(
    private clientsService: ClientsService
  ) {

  }
}
