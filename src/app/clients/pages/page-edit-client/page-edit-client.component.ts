import {Component, Input, OnInit} from '@angular/core';
import {ClientsService} from "../../services/clients.service";
import {Observable} from "rxjs";
import {Clients} from "../../models/clients";
import {faSpinner} from "@fortawesome/free-solid-svg-icons";
import {Router} from "@angular/router";

@Component({
  selector: 'app-page-edit-client',
  templateUrl: './page-edit-client.component.html',
  styleUrls: ['./page-edit-client.component.scss']
})
export class PageEditClientComponent implements OnInit {

  @Input() id!: number;

  client$!: Observable<Clients>;

  constructor(
    private clientsService: ClientsService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.client$ = this.clientsService.getClientById(this.id);
  }

  protected readonly faSpinner = faSpinner;

  goToClientsList() {
    this.router.navigate(['/', 'clients', 'list']);
  }

  updateClient(client: Clients) {
    this.clientsService.putClient({...client, id: this.id}).subscribe(
      (res) => {
        this.goToClientsList();
      });
  }
}
