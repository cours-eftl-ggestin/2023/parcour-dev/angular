import {Component} from '@angular/core';
import {FormBuilder, FormControl, Validators} from "@angular/forms";
import {Clients} from "../../models/clients";
import {ClientsService} from "../../services/clients.service";
import {Router} from "@angular/router";

interface ClientForm {
  name: FormControl<string | null>;
  revenue: FormControl<number | null>;
  status: FormControl<string | null>;
  comments: FormControl<string | null>;
}

@Component({
  selector: 'app-page-add-client',
  templateUrl: './page-add-client.component.html',
  styleUrls: ['./page-add-client.component.scss']
})
export class PageAddClientComponent {

  constructor(
    private clientsService: ClientsService,
    private router: Router
  ) {
  }

  createClient(client: Clients) {
    this.clientsService.postClient(client)
      .subscribe((res) => {
        this.goToClientList();
      });
  }

  goToClientList() {
    this.router.navigate(['/', 'clients', 'list']);
  }
}
