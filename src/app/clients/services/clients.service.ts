import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs";
import {Clients} from "../models/clients";

@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  constructor(
    private http: HttpClient
  ) {
  }

  getClients(): Observable<Clients[]> {
    return this.http.get<Clients[]>('http://localhost:3000/clients');
  }

  getClientById(id: number): Observable<Clients> {
    return this.http.get<Clients>(`http://localhost:3000/clients/${id}`);
  }

  postClient(client: Clients): Observable<Clients> {
    return this.http.post<Clients>('http://localhost:3000/clients', client);
  }

  putClient(client: Clients): Observable<Clients> {
    return this.http.put<Clients>(`http://localhost:3000/clients/${client.id}`, client);
  }
}
