import { Component } from '@angular/core';
import { faEdit } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-icon-edit',
  template: `<fa-icon [icon]="icone"></fa-icon>`,
  styleUrls: ['./icon-edit.component.scss', '../fa-icon.scss']
})
export class IconEditComponent {
  icone = faEdit;
}
