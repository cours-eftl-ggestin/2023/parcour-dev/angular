export interface Order {
  id: number;
  name: string;
  client: string;
  priceHT: number;
  tva: number;
  nbDays: number;
}
