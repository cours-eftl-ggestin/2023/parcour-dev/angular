import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, Validators} from "@angular/forms";
import {Order} from "../../models/order";
import {ClientsService} from "../../../clients/services/clients.service";

interface OrderForm {
  name: FormControl<string | null>;
  client: FormControl<string | null>;
  priceHT: FormControl<number | null>;
  tva: FormControl<number | null>;
  nbDays: FormControl<number | null>;
}

@Component({
  selector: 'app-order-form',
  templateUrl: './order-form.component.html',
  styleUrls: ['./order-form.component.scss']
})
export class OrderFormComponent implements OnInit {
  @Input() order!: Order | null;

  @Output() cancel: EventEmitter<void> = new EventEmitter<void>();
  @Output() validate: EventEmitter<Order> = new EventEmitter<Order>();

  clients$ = this.clientsService.getClients();

  titleButton = 'Ajouter';
  creationOnGoing = false;

  public form = this.fb.group<OrderForm>({
    name: this.fb.control<string | null>(null, [Validators.required]),
    client: this.fb.control<string | null>(null, [Validators.required]),
    priceHT: this.fb.control<number>(0, [Validators.required, Validators.min(0)]),
    tva: this.fb.control<number>(0, [Validators.required, Validators.min(0)]),
    nbDays: this.fb.control<number>(1, [Validators.required, Validators.min(1)]),
  });

  constructor(
    private fb: FormBuilder,
    private clientsService: ClientsService,
  ) { }

  ngOnInit(): void {
    if (this.order) {
      this.titleButton = 'Modifier';
      this.form.patchValue(this.order);
    }
  }

  isNotValid() {
    return this.form.pristine || !this.form.valid;
  }

  isDisabled() {
    return this.isNotValid() && !this.creationOnGoing;
  }

  submit() {
    this.creationOnGoing = true;
    const order: Order = {...this.form.value} as Order;
    this.validate.emit(order);
  }

  cancelAction() {
    this.cancel.emit();
  }

}
