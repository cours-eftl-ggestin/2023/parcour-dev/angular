import { Component } from '@angular/core';
import {Router} from "@angular/router";
import {OrderService} from "../../services/order.service";
import {Order} from "../../models/order";

@Component({
  selector: 'app-page-add-order',
  template: `
    <div class="mt-2 container-fluid">
      <h2 class="text-center">Ajouter une commande</h2>
      <app-order-form
        (cancel)="goToOrderList()"
        (validate)="createOrder($event)"
      >
      </app-order-form>
    </div>
  `,
})
export class PageAddOrderComponent {

  constructor(
    private orderService: OrderService,
    private router: Router
  ) {
  }

  createOrder(order: Order) {
    this.orderService.postOrder(order)
      .subscribe((res) => {
        this.goToOrderList();
      });
  }

  goToOrderList() {
    this.router.navigate(['/', 'orders', 'list']);
  }
}
