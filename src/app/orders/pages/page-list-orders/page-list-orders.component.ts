import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {Clients} from "../../../clients/models/clients";
import {ClientsService} from "../../../clients/services/clients.service";
import {OrderService} from "../../services/order.service";
import {Order} from "../../models/order";

@Component({
  selector: 'app-page-list-orders',
  templateUrl: './page-list-orders.component.html',
  styleUrls: ['./page-list-orders.component.scss']
})
export class PageListOrdersComponent implements OnInit {

  orders$: Observable<Order[]> = this.orderService.data$;
  isloading = true;

  constructor(
    private orderService: OrderService
  ) {

  }

  ngOnInit(): void {
    this.getData();
  }

  private getData() {
    this.orderService.getOrders()
      .subscribe(() => this.isloading = false);
  }

  delete(order: Order) {
    this.orderService.deleteOrder(order)
      .subscribe(() => {
        this.getData();
      });
  }
}
