import {Component, Input} from '@angular/core';
import {Observable} from "rxjs";
import {Router} from "@angular/router";
import {OrderService} from "../../services/order.service";
import {Order} from "../../models/order";
import { faSpinner } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-page-edit-order',
  template: `
    <div class="mt-2 container-fluid">

      <h2 class="text-center">Modifier une commande</h2>

      <ng-container *ngIf="order$ | async as order; else loading">
        <app-order-form
          [order]="order"
          (cancel)="goToOrderList()"
          (validate)="updateOrder($event)"
        ></app-order-form>
      </ng-container>
    </div>

    <ng-template #loading>
      <div class="alert alert-info" role="alert">
        <fa-icon [icon]="faSpinner" animation="spin-pulse"></fa-icon>
        Chargement de la commande ...
      </div>
    </ng-template>
  `,
})
export class PageEditOrderComponent {

  @Input() id!: number;

  order$!: Observable<Order>;

  constructor(
    private orderService: OrderService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.order$ = this.orderService.getOrderById(this.id);
  }

  protected readonly faSpinner = faSpinner;

  goToOrderList() {
    this.router.navigate(['/', 'orders', 'list']);
  }

  updateOrder(order: Order) {
    this.orderService.putOrder({...order, id: this.id}).subscribe(
      (res) => {
        this.goToOrderList();
      });
  }
}
