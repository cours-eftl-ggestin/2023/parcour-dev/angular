import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable, tap} from "rxjs";
import {Order} from "../models/order";

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private data: BehaviorSubject<Order[]> = new BehaviorSubject<Order[]>([]);

  get data$(): Observable<Order[]> {
    return this.data.asObservable();
  }

  constructor(
    private http: HttpClient
  ) {
  }

  getOrders(): Observable<Order[]> {
    return this.http.get<Order[]>('http://localhost:3000/orders')
      .pipe(
        tap((orders) => this.data.next(orders)),
      );
  }

  getOrderById(id: number): Observable<Order> {
    return this.http.get<Order>(`http://localhost:3000/orders/${id}`);
  }

  postOrder(order: Order): Observable<Order> {
    return this.http.post<Order>('http://localhost:3000/orders', order);
  }

  putOrder(order: Order): Observable<Order> {
    return this.http.put<Order>(`http://localhost:3000/orders/${order.id}`, order);
  }

  deleteOrder(order: Order): Observable<void> {
    return this.http.delete<void>(`http://localhost:3000/orders/${order.id}`);
  }
}
