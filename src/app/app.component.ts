import {Component} from '@angular/core';
import {VersionService} from "./core/services/version.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title: string = 'eftl-angular';

  constructor(
    private versionService: VersionService,
  ) {
  }

  onNav(label: string): void {
    console.log(label);
  }

  incrementVersion(props: 'major' | 'minor' | 'patch'): void {
    this.versionService.incrementVersion(props);
  }

  incrementVersionMajor() {
    this.versionService.incrementVersionMajor();
  }

  incrementVersionMinor() {
    this.versionService.incrementVersionMinor();
  }

  incrementVersionPatch() {
    this.versionService.incrementVersionPatch();
  }


}
